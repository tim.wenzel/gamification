import { VcsConnection } from './vcsConnection';
import { Constants } from '../constants';

/**
 * Implementation of connection to online GitLab service via web API
 *
 * @author Tim Wenzel
 */
export class GitLabConnection extends VcsConnection {

    /* GitLab ID of project to connect with */
    projectId: number;
    /* Personal token of user to connect to private areas of GitLab */
    userToken: string;
    /* ID of GitLab group in case project belongs to a group */
    groupId?: number;

    constructor(projectId: number, userToken: string, groupId?: number) {
        super('https://gitlab.com/api/v4/projects/' + projectId);
        this.projectId = projectId;
        this.userToken = userToken;
        this.groupId = groupId;
    }

    /**
     * Returns number of commits by specific user (as promise)
     * @param user User to filter by
     * @param page Target page of API response (see GitLab pagination: https://docs.gitlab.com/ee/api/#pagination)
     * @param counter Number of registered commits (in case of pagination and recursive calls)
     */
    async getCommitsByUser(user: string, page: number, counter: number): Promise<number> {
        return new Promise((resolve, reject) => {
            const request = new XMLHttpRequest();
            request.open('GET', this.hostUrl + '/repository/commits?per_page=100&page=' + page);
            // request.setRequestHeader('PRIVATE-TOKEN', this.userToken);
            request.onreadystatechange = () => {
                if (request.readyState === XMLHttpRequest.DONE) {
                    const pages = Number(request.getResponseHeader('X-Total-Pages'));

                    const response: any = JSON.parse(request.responseText);
                    if (response.message && (response.message.includes('401') || response.message.includes('404'))) {
                        reject(new Error('Authorization'));
                    }
                    response.forEach(obj => { if (obj.committer_name === user) { counter++; } });

                    if (page < pages) {
                        resolve(this.getCommitsByUser(user, page + 1, counter));
                    } else {
                        resolve(counter);
                    }
                }
            };
            request.send();
        });
    }

    /**
     * Returns number of completed issues by specific user (as promise)
     * @param user User to filter by
     * @param page Target page of API response (see GitLab pagination: https://docs.gitlab.com/ee/api/#pagination)
     * @param counter Number of registered commits (in case of pagination and recursive calls)
     */
    async getCompletedIssuesByUser(user: string, page: number, counter: number): Promise<number> {
        return new Promise((resolve, reject) => {
            const request = new XMLHttpRequest();
            request.open('GET', this.hostUrl + '/issues?per_page=100&page=' + page);
            // request.setRequestHeader('PRIVATE-TOKEN', this.userToken);
            request.onreadystatechange = () => {
                if (request.readyState === XMLHttpRequest.DONE) {
                    const pages = Number(request.getResponseHeader('X-Total-Pages'));

                    const response: any = JSON.parse(request.responseText);
                    if (response.message && (response.message.includes('401') || response.message.includes('404'))) {
                        reject(new Error('Authorization'));
                    }
                    response.forEach(obj => { if (obj.assignee.name === user && obj.state === 'closed') { counter++; } });

                    if (page < pages) {
                        resolve(this.getCompletedIssuesByUser(user, page + 1, counter));
                    } else {
                        resolve(counter);
                    }
                }
            };
            request.send();
        });
    }

    async getCompletedIssuesByTeam(team: string[]): Promise<number> {
        throw new Error('Teams not included yet.');
    }

    async getMergeRequestsByUser(user: string, page: number, counter: number): Promise<number> {
        // TODO: Works as expected? - Needs to be checked.
        return new Promise((resolve, reject) => {
            const request = new XMLHttpRequest();
            request.open('GET', this.hostUrl + '/merge_requests?per_page=100&page=' + page);
            // request.setRequestHeader('PRIVATE-TOKEN', this.userToken);
            request.onreadystatechange = () => {
                if (request.readyState === XMLHttpRequest.DONE) {
                    const pages = Number(request.getResponseHeader('X-Total-Pages'));

                    const response: any = JSON.parse(request.responseText);
                    if (response.message && (response.message.includes('401') || response.message.includes('404'))) {
                        reject(new Error('Authorization'));
                    }
                    response.forEach(obj => {
                        if (obj.state === 'closed' || obj.state === 'merged') {
                            for (const assignee of obj.assignees) {
                                if (assignee === user) {
                                    counter++;
                                }
                            }
                        }
                    });

                    if (page < pages) {
                        resolve(this.getCommitsByUser(user, page + 1, counter));
                    } else {
                        resolve(counter);
                    }
                }
            };
            request.send();
        });
    }

    async getMergesByUser(user: string, page: number, counter: number): Promise<number> {
        // TODO: Implementation
        throw new Error('Method not implemented.');
    }

    async getMilestonesByTeam(team: string[]): Promise<number> {
        throw new Error('Teams not included yet.');
    }

    async getMilestonesByUser(user: string, page: number, counter: number): Promise<number> {
        // TODO: Implementation
        throw new Error('Method not implemented.');
    }

    /**
     * Returns lines of code by specific user (as promise)
     * All changes count here: Added, changed, deleted lines
     * @param user User to filter by
     * @param page Target page of API response (see GitLab pagination: https://docs.gitlab.com/ee/api/#pagination)
     * @param counter Number of registered commits (in case of pagination and recursive calls)
     */
    async getLocForUser(user: string, page: number, counter: number): Promise<number> {
        return new Promise((resolve, reject) => {
            const request = new XMLHttpRequest();
            request.open('GET', this.hostUrl + '/repository/commits?with_stats=true&per_page=100&page=' + page);
            // request.setRequestHeader('PRIVATE-TOKEN', this.userToken);
            request.onreadystatechange = () => {
                if (request.readyState === XMLHttpRequest.DONE) {
                    try {
                        const pages = Number(request.getResponseHeader('X-Total-Pages'));

                        const response: any = JSON.parse(request.responseText);
                        if (response.message && (response.message.includes('401') || response.message.includes('404'))) {
                            reject(new Error('Authorization'));
                        }
                        response.forEach(obj => {
                            // Second condition prevents recognition of bootstrapping / initial commit.
                            if (obj.committer_name === user && !this.containsRejectedString(obj.message)) {
                                counter += obj.stats.total;
                            }
                        });

                        if (page < pages) {
                            resolve(this.getLocForUser(user, page + 1, counter));
                        } else {
                            resolve(counter);
                        }
                    } catch (e) {
                        reject(new Error('Parsing'));
                    }
                }
            };
            request.send();
        });
    }

    /**
     * Filters for rejected string in GitLab commit messages
     * @param message String to filter
     */
    private containsRejectedString(message: any): boolean {
        for (const s of Constants.REJECTED_STRINGS) {
            if (message.includes(s)) {
                return true;
            }
        }
        return false;
    }

    async getLocForTeam(team: string[]): Promise<number> {
        throw new Error('Teams not included yet.');
    }

}
