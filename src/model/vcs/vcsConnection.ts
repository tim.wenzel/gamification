/**
 * Abstract class for version control connections
 *
 * @author Tim Wenzel
 */
export abstract class VcsConnection {

    /* Host IP (incl. port) */
    hostUrl: string;

    constructor(hostUrl: string) {
        this.hostUrl = hostUrl;
    }

    abstract async getCommitsByUser(user: string, page: number, counter: number): Promise<number>;

    abstract async getCompletedIssuesByUser(user: string, page: number, counter: number): Promise<number>;
    abstract async getCompletedIssuesByTeam(team: string[]): Promise<number>;

    abstract async getMergeRequestsByUser(user: string, page: number, counter: number): Promise<number>;

    abstract async getMergesByUser(user: string, page: number, counter: number): Promise<number>;

    abstract async getMilestonesByTeam(team: string[]): Promise<number>;
    abstract async getMilestonesByUser(user: string, page: number, counter: number): Promise<number>;

    abstract async getLocForUser(user: string, page: number, counter: number): Promise<number>;
    abstract async getLocForTeam(team: string[]): Promise<number>;

}
