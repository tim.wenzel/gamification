import { Constants } from '../constants';

/**
 * Connection to SonarQube server
 *
 * @author Tim Wenzel
 */
export class SonarqubeConnection {

    /* Project key specified in SonarQube settings */
    projectId: string;
    /* Host IP (incl. port) */
    host: string;

    constructor(host: string, projectId: string) {
        this.host = 'http://' + host + '/api/';
        this.projectId = projectId;
    }

    /**
     * Get all issues identified by SonarQube via XMLHttpRequest
     */
    async getIssuesByProject(): Promise<any[]> {
        return new Promise((resolve, reject) => {
            const request = new XMLHttpRequest();
            request.open('GET', this.host + 'issues/search?componentKeys=' + this.projectId);
            request.onreadystatechange = () => {
                if (request.readyState === XMLHttpRequest.DONE) {
                    try {
                        const list = JSON.parse(request.responseText).issues;
                        resolve(list);
                    } catch { reject(new Error('Connection')); }
                }
            };
            request.send();
        });
    }

    /**
     * Get all bugs identified by SonarQube via Issues
     */
    async getBugsByUser(user: string): Promise<number> {
        return new Promise((resolve, reject) => {
            this.getIssuesByProject().then(list => {
                let counter = 0;
                list.forEach(elem => {
                    if (elem.type === 'RELIABILITY' && elem.status === 'CLOSED') { counter++; }
                });
                resolve(counter);
            }).catch(err => reject(err));
        });
    }

    async getBugsByTeam(team: string[]): Promise<number> {
        throw new Error('Teams not included yet.');
    }

    async getSecurityByUser(user: string): Promise<number> {
        // TODO: Mocked value (No security issues in current project.)
        return new Promise(resolve => setTimeout(() => resolve(26), Constants.DEFAULT_TIMEOUT));
    }

    async getSecurityByTeam(team: string[]): Promise<number> {
        throw new Error('Teams not included yet.');
    }

    /**
     * Get all code smells identified by SonarQube via Issues
     */
    async getCodeSmellsByUser(user: string): Promise<number> {
        return new Promise((resolve, reject) => {
            this.getIssuesByProject().then(list => {
                let counter = 0;
                for (const elem of list) {
                    if (elem.type === 'CODE_SMELL' && elem.status === 'CLOSED') { counter++; }
                }
                resolve(counter);
            }).catch(err => reject(err));
        });
    }

    async getCodeSmellsByTeam(team: string[]): Promise<number> {
        throw new Error('Teams not included yet.');
    }

    /**
     * Get all code cuplications identified by SonarQube via XMLHttpRequest
     */
    async getCodeDuplicationsByUser(user: string): Promise<number> {
        // TODO: Current state: Available duplications to fix. Target state: Already fixed duplications.
        return new Promise((resolve, reject) => {
            this.getAllTsFiles().then(files => {
                let duplications = 0;
                for (const file of files) {
                    const request = new XMLHttpRequest();
                    request.open('GET', this.host + 'duplications/show?key=' + this.projectId + ':' + file);
                    request.onreadystatechange = () => {
                        if (request.readyState === XMLHttpRequest.DONE) {
                            const object = JSON.parse(request.responseText);
                            duplications += object.duplications.length;
                        }
                    };
                    request.send();
                }
                setTimeout(() => {
                    resolve(duplications);
                }, Constants.DEFAULT_TIMEOUT);
            }).catch(err => reject(err));
        });
    }

    async getCodeDuplicationsByTeam(team: string[]): Promise<number> {
        throw new Error('Teams not included yet.');
    }

    async getCycloComplexityByUser(user: string): Promise<number> {
        // TODO: Implementation
        throw new Error('Method not implemented.');
    }

    async getCognitiveComplexityByTeam(team: string[]): Promise<number> {
        throw new Error('Teams not included yet.');
    }

    async getCognitiveComplexityByUser(user: string): Promise<number> {
        // TODO: Implementation
        throw new Error('Method not implemented.');
    }

    /**
     * Get all files processed by SonarQube via XHR
     */
    private async getFileTree(): Promise<any> {
        return new Promise((resolve, reject) => {
            const request = new XMLHttpRequest();
            request.open('GET', this.host + 'components/tree?component=' + this.projectId);
            request.onreadystatechange = () => {
                if (request.readyState === XMLHttpRequest.DONE) {
                    try {
                        const list = JSON.parse(request.responseText).components;
                        resolve(list);
                    } catch { reject(new Error('Connection')); }
                }
            };
            request.send();
        });
    }

    /**
     * Filter for all Typescript TS files
     */
    private async getAllTsFiles(): Promise<any> {
        return new Promise((resolve, reject) => {
            const files = [];

            this.getFileTree().then(list => {
                for (const elem of list) {
                    if (elem.language === 'ts') { files.push(elem.path); }
                }
                resolve(files);
            }).catch(err => reject(err));
        });
    }

}
