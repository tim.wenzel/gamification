import { Constants } from '../constants';

/**
 * Items to collect during gamification session.
 *
 * @author Tim Wenzel
 */
export class Item {

    name: string;
    value: number;
    iconPath: string;

    /**
     * @param name Item name
     * @param value Item value (value from 0 and 1)
     * @param iconName Name of icon file, stored in 'src/assets/images/items/'
     */
    constructor(name: string, value: number, iconName: string) {
        if (value > 1) { throw new Error('Item value must not be greater than 1!'); }

        this.name = name;
        this.value = value;
        this.iconPath = '../../assets/images/items/' + iconName + '.png';
    }

    /**
     * Returns random item with value between 0.6 and 0.9
     */
    static getHighValueItem(): Item {
        const rand = Math.floor(this.getRandomNumber(0, Constants.ITEMS_HIGH.length));
        return new Item(Constants.ITEMS_HIGH[rand][0], this.getRandomNumber(0.6, 0.9001), Constants.ITEMS_HIGH[rand][1]);
    }

    /**
     * Returns random item with value between 0.3 and 0.7
     */
    static getMediumValueItem(): Item {
        const rand = Math.floor(this.getRandomNumber(0, Constants.ITEMS_MEDIUM.length));
        return new Item(Constants.ITEMS_MEDIUM[rand][0], this.getRandomNumber(0.3, 0.7001), Constants.ITEMS_MEDIUM[rand][1]);
    }

    /**
     * Returns random item with value between 0.1 and 0.4
     */
    static getLowValueItem(): Item {
        const rand = Math.floor(this.getRandomNumber(0, Constants.ITEMS_LOW.length));
        return new Item(Constants.ITEMS_LOW[rand][0], this.getRandomNumber(0.1, 0.4001), Constants.ITEMS_LOW[rand][1]);
    }

    /**
     * Returns random number in range
     * @param max Maximum value
     * @param min Minimum value
     */
    private static getRandomNumber(max: number, min: number): number {
        const rand = this.round(Math.random() * (max - min) + min, 2);
        return rand;
    }

    /**
     * Returns rounded value
     * @param value Value to round
     * @param dec Number of decimals
     */
    private static round(value: number, dec: number): number {
        const factor = Math.pow(10, dec);
        return Math.round(value * factor) / factor;
    }

    // reduceValue(diff: number): void {
    //     if (diff > 1 || diff >= this.value) {
    //         throw new Error('Invalid reduce value. Value must be less than 1 and less than current item value.');
    //     }

    //     this.value -= diff;
    // }

}
