import { Player } from 'src/model/game/player';

/**
 * User entity
 *
 * @author Tim Wenzel
 */
export class User {

    id: number;
    loginName: string;
    password: string;
    lastName: string;
    firstName: string;

    player: Player;

    userToken: string;

    constructor(id: number, loginName: string, password: string, firstName: string, lastName: string) {
        this.id = id;
        this.loginName = loginName;
        this.password = password;
        this.lastName = lastName;
        this.firstName = firstName;

        this.player = new Player();
    }

}

/**
 * Service to manage users in application
 *
 * @author Tim Wenzel
 */
export class UserService {

    /**
     * Update user object after changeing data
     * @param user New user object with changed values
     */
    static updateUser(user: User) {
        const users = UserService.getUsers();

        users.forEach(u => {
            if (u.loginName === user.loginName) {
                users[users.indexOf(u)] = user;
            }
        });

        UserService.setUsers(users);
    }

    /**
     * Returns all registered users
     */
    static getUsers(): User[] {
        return JSON.parse(localStorage.getItem('users') || '[]');
    }

    /**
     * Returns currently logged in user
     */
    static getCurrentUser(): User {
        return JSON.parse(localStorage.getItem('currentUser') || 'null');
    }

    /**
     * (Re)Sets all users in the application
     * @param users Set of users
     */
    static setUsers(users: User[]): void {
        localStorage.setItem('users', JSON.stringify(users));

        if (localStorage.getItem('currentUser')) {
            this.setCurrentUser(users.filter(u => u.loginName === this.getCurrentUser().loginName)[0]);
        }
    }

    /**
     * (Re)Sets current user
     * @param user User object to be the current user
     */
    static setCurrentUser(user: User): void {
        localStorage.setItem('currentUser', JSON.stringify(user));
    }

    /**
     * Finds user by given login name
     * @param loginName Login name to be filtered by
     */
    static findUserByLogin(loginName: string): User | undefined {
        const users: User[] = this.getUsers();

        for (const u of users) {
            if (u.loginName === loginName) {
                return u;
            }
        }

        return undefined;
    }

}
