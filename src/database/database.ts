import { User, UserService } from 'src/login/userService';
import { Item } from 'src/model/game/item';

/**
 * Management of embedded database / local browser storage
 *
 * @author Tim Wenzel
 */
export class Database {

    /**
     * Initialization of storage at application startup
     */
    init(): void {
        localStorage.setItem('running', 'true');

        this.initUsers();
        this.initItems();
    }

    /**
     * Initialization of users
     */
    private initUsers(): void {
        const users = [
            new User(0, 'admin', 'admin', 'Admin', ''),
            new User(1, 'tim', 'tim123', 'Tim', 'Wenzel'),
            new User(2, 'max', 'max123', 'Max', 'Mustermann'),
            new User(3, 'test', 'test123', 'Test', 'Person'),
            new User(4, 'johndoe', 'johndoe123', 'John', 'Doe'),
            new User(5, 'thomas', 'thomas123', 'Thomas', 'Schmidt')
        ];

        UserService.setUsers(users);
    }

    /**
     * Initialization of items
     */
    private initItems(): void {
        const users: User[] = UserService.getUsers();

        users.forEach(user => {
            for (let i = 0; i < Math.floor(Math.random() * 10); i++) {
                const rand = Math.floor(Math.random() * 3);
                if (rand === 0) {
                    user.player.items.push(Item.getLowValueItem());
                } else if (rand === 1) {
                    user.player.items.push(Item.getMediumValueItem());
                } else {
                    user.player.items.push(Item.getHighValueItem());
                }
            }
        });

        UserService.setUsers(users);
    }

}
