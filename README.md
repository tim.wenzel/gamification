# Gamification-Prototyp
Prototyp für ein Gamification-Konzept im Rahmen einer Bachelorarbeit an der Westsächsischen Hochschule Zwickau.  
**Thema:** Analyse von Evaluationskriterien für den Einsatz von Gamification in der Softwareentwicklung  
(Wintersemester 2019/20)

## Anwendung / Prototyp
### Ziel
Vermittlung eines allgemeinen Eindrucks für die Nutzung von Gamification.  
Anwendung speziell im Bereich der Softwareentwicklung.

### Voraussetzungen
* [NodeJS](https://nodejs.org/en/)
* [GitLab](https://gitlab.com/)-Account
* [SonarQube](https://www.sonarqube.org/downloads/)
* IDE: [Microsoft Visual Studio Code](https://code.visualstudio.com/) (oder vergleichbare)

### Setup
1. Installationen (s. Voraussetzungen)
2. Repository klonen
3. 'data'-Order in SonarQube-Installation mit 'data'-Ordner in höchster Repository-Ebene ersetzen (embedded DB)
4. [CORS](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS)-Konfiguration für den SonarQube-Server:
    * Datei: <SonarQube_PFAD>/web/WEB-INF/web.xml
    * einzufügender Code:
```
<filter>
  <filter-name>CorsFilter</filter-name>
  <filter-class>org.apache.catalina.filters.CorsFilter</filter-class>
  <init-param>
    <param-name>cors.allowed.origins</param-name>
    <param-value>*</param-value>
  </init-param>
</filter>

<filter-mapping>
  <filter-name>CorsFilter</filter-name>
  <url-pattern>/*</url-pattern>
</filter-mapping>
```
4. geklonten Repository-Ordner in Visual Studio Code oder anderer IDE öffnen
5. SonarQube-Server starten (in Powershell):
    * `<SonarQube_PFAD>\bin\windows-x86-64\StartSonar.bat` (für Windows)
    * oder: `<SonarQube_PFAD>/bin/<BETRIEBSSYSTEM>/StartSonar` (für andere Betriebssysteme)
5. Terminal -> New Terminal (in VS Code)
6. `npm install`
6. Anwendung starten:
    * Nutzung nur im eigenen Browser: `ionic serve`
        * Zugriff via Broser: `localhost:8100`
    * Nutzung via Netzwerk: `ionic serve --address 0.0.0.0`
        * Zugriff via Netzwerk: `<HOST_IP>:8100`

### Nutzung der Anwendung
**Loginseite:** (Credentials)
* tim : tim123 (GitLab-Account-Linking)
* max : max123
* test : test123
* johndoe : johndoe123
* admin : admin

**Dashboard-Seite:**
* ~~Token: *2w1eLGvWqxunFxnwdRrv* [Beispieltoken]~~
    * ~~Token wird später ungültig sein - eigenes Token erstellen: https://gitlab.com/profile/personal_access_tokens~~
* ~~eintragen, dann \<ENTER\>~~  
> Hinweis: In diesem öffentlichen Beispielprojekt ist die Nutzung eines Tokens nicht nötig / möglich.

**Fortschritt-Seite:**
* gewünschte Metrik auswählen
* ggf. Belohnungen sammeln
* zurück / Dashboard

**Wettbewerb-Seite:**
* wenn für jeden Nutzer *Nicht Bereit*:
    * *Logout* -> mit anderem Nutzer einloggen -> im Dashboard *Herausforderungen annehmen* umschalten
* wo möglich: *Herausfordern* drücken
* Ergebnis abwarten
* Freuen :-)
* oder ärgern :-(

### Anwendung zurücksetzen
* Möglichkeit 1: *Logout* -> *Anwendung zurücksetzen*  
* Möglichkeit 2: Lokalen Browser-Speicher löschen
    * Firefox (im Anwendungs-Tab):
    * F12 -> Web-Speicher -> Local Storage -> localhost:8100 -> Rechtsklick -> Alles löschen

## SonarQube
* Prüfen der hinterlegten Daten:
* SonarQube-Server starten (s. Setup)
* Browser: localhost:9000
* Login:
    * admin : admin